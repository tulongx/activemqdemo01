package com.ly.customer.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author linyun
 * @date 2018/12/2 14:22
 */
@Slf4j
@EnableJms
@Component
public class Customer {
    @JmsListener(destination = "queue01", containerFactory = "queueListenerFactory")
    public void customer(String msg) {
        System.out.println("接收到的消息:");
        System.out.println(msg);
    }

    @JmsListener(destination = "delaySend01", containerFactory = "queueListenerFactory")
    public void customer2(String msg) {
        log.info("接收延时消息:" + msg);
    }

    @JmsListener(destination = "topic01", containerFactory = "topicListenerFactory")
    public void customer3(String msg) {
        log.info("收到订阅消息:" + msg);
    }

}
