package com.ly.producter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableJms
@SpringBootApplication
public class ProducterApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProducterApplication.class, args);
    }
}
