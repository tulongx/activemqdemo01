package com.ly.producter.api;

import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.jms.Destination;

/**
 * @author linyun
 * @date 2018/12/2 13:11
 */
@RestController
public class Producter {

    private final ActiveMQHandler handler;

    @Autowired
    public Producter(ActiveMQHandler handler) {
        this.handler = handler;
    }

    /**
     * 即时消息
     *
     * @return
     */
    @RequestMapping("/queue")
    public String queue() {
        // 构建一个消息, 名称是 queue01
        String message = "我是消息内容, " + System.currentTimeMillis();
        handler.send("queue01", message);
        return "success";
    }

    /**
     * 延迟消息
     *
     * @return
     */
    @RequestMapping("/delaySend")
    public String delaySend() {
        // 构建一个消息, 名称是 queue01
        for (int i = 0; i < 30; i++) {
            String message = "我是延迟消息内容, " + i;
            handler.delaySend("delaySend01", message, 10);
        }
        return "success";
    }

    @RequestMapping("/topic")
    public String topic(){
        handler.topic("topic01","hello world");
        return "success";
    }
}
